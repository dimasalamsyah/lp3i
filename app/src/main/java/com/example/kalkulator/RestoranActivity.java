package com.example.kalkulator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class RestoranActivity extends AppCompatActivity {

    private EditText etmakanan, etqty, etharga;
    private Button btnsimpan;
    private int no = 0;

    //adapter
    private RecyclerView recyclerView;
    private RiwayatAdapter adapter;
    private ArrayList<String> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restoran);

        etmakanan = findViewById(R.id.etmakanan);
        etqty = findViewById(R.id.etqty);
        etharga = findViewById(R.id.etharga);
        btnsimpan = findViewById(R.id.btnsimpan);

        //inisialisasi recycleview
        recyclerView=findViewById(R.id.recyclerView);
        adapter = new RiwayatAdapter(this, list);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        String nama = getIntent().getStringExtra("nama");
        String pelajaran = getIntent().getStringExtra("pelajaran");
        int nilai = getIntent().getIntExtra("nilai", 0);

        //event klik button
        btnsimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // ambil data
                String makanan = etmakanan.getText().toString();
                String qty = etqty.getText().toString();
                String harga = etharga.getText().toString();

                no++;
                list.add(no +", "+ makanan );

                refresh();

                //adapter.notifyDataSetChanged();
            }
        });
    }

    public void refresh(){

        ArrayList<String> listBaru = new ArrayList<String>();

        for (int i = 0; i < list.size(); i++){

            String[] split = list.get(i).split(",");

            listBaru.add(i +", "+ split[1].toString().trim() );
        }

        list = listBaru;

        adapter = new RiwayatAdapter(this, listBaru);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    public void load(ArrayList<String> listBaru){
        list.clear();

        adapter = new RiwayatAdapter(this, listBaru);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}
