package com.example.restoran;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.kalkulator.R;

public class RegisterRestoranActivity extends AppCompatActivity {

    private ImageView btnBack;
    private DBHelper dbHelper;
    private EditText etUsername;
    private EditText etPassword;
    private Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_restoran);

        //iniasis database
        dbHelper = new DBHelper(this);

        btnBack = findViewById(R.id.imgBack);
        etUsername = findViewById(R.id.etUsername);
        etPassword = findViewById(R.id.etPassword);
        btnRegister = findViewById(R.id.btnRegister);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Gets the data repository in write mode
                SQLiteDatabase db = dbHelper.getWritableDatabase();

                // Create a new map of values, where column names are the keys
                ContentValues values = new ContentValues();
                values.put(Table.Users.NAMA, etUsername.getText().toString() );
                values.put(Table.Users.PASSWORD, etPassword.getText().toString() );

                // Insert the new row, returning the primary key value of the new row
                long newRowId = db.insert(Table.Users.TABLE_NAME, null, values);

                if (newRowId > 0){
                    finish();
                }
            }
        });

    }
}
