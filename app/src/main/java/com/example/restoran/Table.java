package com.example.restoran;

import android.provider.BaseColumns;

public class Table {

    public Table() {
    }

    /* Inner class that defines the table contents */
    public static class Users implements BaseColumns {
        public static final String TABLE_NAME = "m_users";
        public static final String NAMA = "nama";
        public static final String PASSWORD = "password";
    }

}
