package com.example.restoran;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "restoran.db";
    private static final int DATABASE_VERSION = 1;

    private static final String SQL_CREATE_M_USERS =
            "CREATE TABLE " + Table.Users.TABLE_NAME + " (" +
                    Table.Users._ID + " INTEGER PRIMARY KEY," +
                    Table.Users.NAMA + " TEXT," +
                    Table.Users.PASSWORD + " TEXT)";

    private static final String SQL_DELETE_M_USERS =
            "DROP TABLE IF EXISTS " + Table.Users.TABLE_NAME;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(SQL_CREATE_M_USERS);

    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_M_USERS);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }


}
